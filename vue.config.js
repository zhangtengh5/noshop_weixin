module.exports = {
  publicPath: 'production' === process.env.NODE_ENV ? './' : '/',
  devServer: {
    proxy: {
      '/api': {
        target: 'http://test.zhanggou.zh2zh.com/',
        changeOrigin: true,
        pathRewrite: {
          '^/api': '/'
        }
      }
    }
  },
  lintOnSave: false
};