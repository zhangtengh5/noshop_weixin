import Toast from './toast.vue'
export default {
  install(Vue: any) {
    //扩展实例构造器
    let ToastConstructor = Vue.extend(Toast);
    // 实例化一个 toast.vue
    let instance = new ToastConstructor();
    instance.$mount();
    document.body.appendChild(instance.$el);
    Vue.prototype.$toast = (options: any) => {
      instance.message = options;
      instance.visible = true;
    }
  }
}
declare module 'vue/types/vue' {
  interface Vue {
    $toast: (options: any) => void
  }
}


